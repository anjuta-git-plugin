/*
 * dnd.h - Header file for dnd.c.
 *
 * Copyright (C) 2000 Jos� Antonio Caminero Granja
 *
 * Author(s): 
 * 	Jos� Antonio Caminero Granja <JCamGra@alumnos.uva.es>
 *  Archit Baweja <bighead@crosswinds.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#include <gnome.h>

#ifndef _DND_H_
#define _DND_H_

/*
 * Maximum number of mime types that the widget can handle droppings for.
 */
#define DND_MAX_MIME_TYPES 20

/*
 * dnd_drop_init () - Initialize a widget to accept "droppings".
 *	
 * - GtkWidget *widget
 * 	Widget which will respond to the drags a.k.a "droppings".
 * - void (* data_dropped) (gchar *file_name, gpointer data)
 *	A pointer to the user supplied function which will handle the
 *      "droppings" (file_name).
 * - gpointer data
 *      Any user supplied data. This will be passed on to the callback. 
 * - ... [char *mime_type]
 *	NULL terminated list of accepted mime types.
 */
void dnd_drop_init (GtkWidget *widget,
	 				void (* data_dropped) (const gchar *uri, gpointer data),
					gpointer data, ...);

void dnd_drop_finalize (GtkWidget *widget, gpointer user_data);

#endif	/* _DND_H_ */
