
#ifndef __vg_marshal_MARSHAL_H__
#define __vg_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* NONE:NONE (./vgmarshal.list:1) */
#define vg_marshal_VOID__VOID	g_cclosure_marshal_VOID__VOID
#define vg_marshal_NONE__NONE	vg_marshal_VOID__VOID

/* NONE:INT (./vgmarshal.list:2) */
#define vg_marshal_VOID__INT	g_cclosure_marshal_VOID__INT
#define vg_marshal_NONE__INT	vg_marshal_VOID__INT

/* NONE:POINTER (./vgmarshal.list:3) */
#define vg_marshal_VOID__POINTER	g_cclosure_marshal_VOID__POINTER
#define vg_marshal_NONE__POINTER	vg_marshal_VOID__POINTER

G_END_DECLS

#endif /* __vg_marshal_MARSHAL_H__ */

